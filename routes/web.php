<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Auth::routes();
Route::get('/', function () {
    return view('welcome');
});

Route::get('/organizers', 'App\Http\Controllers\OrganizersController@index'); // NOTE: Add auth middleware eventually.
Route::get("/organizer/addOrUpdate/{organizer_id}", 'App\Http\Controllers\OrganizersController@addOrUpdate');
Route::get("/organizer/{organizer_id}/fetchEvents", 'App\Http\Controllers\EventsController@fetchEventsFromEventbrite');

Route::post("/api/organizer", "App\Http\Controllers\ApiOrganizerController@store");
Route::get("/api/fetch-events", "App\Http\Controllers\ApiEventsController@fetchAllEvents");
Route::get("/api/events", "App\Http\Controllers\ApiEventsController@index");
