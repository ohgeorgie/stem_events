<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Organizer
 *
 * @property int $id
 * @property string|null $description
 * @property string|null $description_html
 * @property string|null $long_description
 * @property string|null $long_description_html
 * @property string|null $resource_uri
 * @property string|null $name
 * @property string|null $url
 * @property string|null $vanity_url
 * @property string|null $twitter
 * @property string|null $facebook
 * @property string|null $logo_id
 * @property int $is_active
 * @property string|null $last_scraped
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\StemEvent[] $stemEvents
 * @property-read int|null $stem_events_count
 * @method static \Illuminate\Database\Eloquent\Builder|Organizer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Organizer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Organizer query()
 * @method static \Illuminate\Database\Eloquent\Builder|Organizer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organizer whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organizer whereDescriptionHtml($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organizer whereFacebook($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organizer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organizer whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organizer whereLastScraped($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organizer whereLogoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organizer whereLongDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organizer whereLongDescriptionHtml($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organizer whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organizer whereResourceUri($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organizer whereTwitter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organizer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organizer whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organizer whereVanityUrl($value)
 */
	class Organizer extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\StemEvent
 *
 * @property int $id
 * @property int|null $organizer_id
 * @property string|null $name
 * @property string|null $name_html
 * @property string|null $description
 * @property string|null $description_html
 * @property string|null $summary
 * @property string|null $start_local
 * @property string|null $start_utc
 * @property string|null $start_tz
 * @property string|null $end_local
 * @property string|null $end_utc
 * @property string|null $end_tz
 * @property int $online_event
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Organizer|null $organizer
 * @method static \Illuminate\Database\Eloquent\Builder|StemEvent newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StemEvent newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StemEvent query()
 * @method static \Illuminate\Database\Eloquent\Builder|StemEvent whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StemEvent whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StemEvent whereDescriptionHtml($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StemEvent whereEndLocal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StemEvent whereEndTz($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StemEvent whereEndUtc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StemEvent whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StemEvent whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StemEvent whereNameHtml($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StemEvent whereOnlineEvent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StemEvent whereOrganizerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StemEvent whereStartLocal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StemEvent whereStartTz($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StemEvent whereStartUtc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StemEvent whereSummary($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StemEvent whereUpdatedAt($value)
 */
	class StemEvent extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $two_factor_secret
 * @property string|null $two_factor_recovery_codes
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Sanctum\PersonalAccessToken[] $tokens
 * @property-read int|null $tokens_count
 * @method static \Database\Factories\UserFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereTwoFactorRecoveryCodes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereTwoFactorSecret($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

