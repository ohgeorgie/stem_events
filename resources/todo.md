# TODO

- [ ] page to "Add Organizer" or modify an existing one.
  - [ ] Maybe paste the organizer eventbrite url and it was parse out the number at the end?
  - [ ] Organizers should be "Active" or "Inactive" to avoid checking inactive ones for events.
  - [ ] Periodically update the organizers one by one. If  "updated_at" is more than 1 month? 2 months? update all fields
- [ ] Add Vuejs
- [ ] Add Vuecal
- [ ] Add login page - default bootstrap for now?
- [ ] moment.tz.js for timezones.. ugh
