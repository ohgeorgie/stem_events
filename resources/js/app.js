require('./bootstrap');

import { createApp } from 'vue'
window.moment = require('moment')
import moment from 'moment';

const app = createApp({})

import Calendar from './components/Calendar.vue'
import OrganizersList from './components/OrganizersList.vue'

app.component('calendar', Calendar)
app.component('organizers-list', OrganizersList)

app.config.globalProperties.$moment=moment;

app.mount('#app')
