<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('pageTitle', env('APP_NAME'))</title>
    {{-- <script src="https://polyfill.io/v3/polyfill.min.js?features=es2015%2Ces2016%2Ces2017%2Ces2018%2Ces2019"></script> --}}
    <script>
        window.App = {!! json_encode([
			'csrfToken' => csrf_token(),
			'signedIn' => Auth::check(),
			'user' => Auth::user(),
		]) !!}
    </script>

    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    @yield('css')
    <link rel="shortcut icon" href="/favicon.ico">
</head>

<body>

    <div id="app">
        @if (!in_array(Route::current()->getName(), ['register', 'login', 'password.reset', 'password.request', 'password.change'], true))
            @include('layouts.nav')
        @endif

        @yield('content')

        {{-- <flash message="{{ session('flash') }}"></flash> --}}

        {{-- @if (!in_array(Route::current()->getName(), ['register', 'login', 'password.reset', 'password.request', 'password.change'], true))
            @include('layouts.footer')
        @endif --}}
    </div>
    <script src="{{ mix('js/manifest.js') }}"></script>
    <script src="{{ mix('js/vendor.js') }}"></script>
    <script src="{{ mix('js/app.js') }}"></script>
    @yield('js')

    <script>
        // navbar burger
        document.addEventListener('DOMContentLoaded', function() {

            // Get all "navbar-burger" elements
            var $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

            // Check if there are any navbar burgers
            if ($navbarBurgers.length > 0) {

                // Add a click event on each of them
                $navbarBurgers.forEach(function($el) {
                    $el.addEventListener('click', function() {

                        // Get the target from the "data-target" attribute
                        var target = $el.dataset.target;
                        var $target = document.getElementById(target);

                        // Toggle the class on both the "navbar-burger" and the "navbar-menu"
                        $el.classList.toggle('is-active');
                        $target.classList.toggle('is-active');

                    });
                });
            }

        });
    </script>

    @yield('js')
</body>

</html>