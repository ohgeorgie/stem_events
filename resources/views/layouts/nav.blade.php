<nav class="navbar" role="navigation" aria-label="main navigation">
    <div class="navbar-brand">
        <a class="navbar-item" href="#">
            Stem Events
            {{-- <img src="https://bulma.io/images/bulma-logo.png" width="112" height="28"> --}}
        </a>

        <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbar">
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
        </a>
    </div>

    <div id="navbar" class="navbar-menu">
        <div class="navbar-start">
            <span class="navbar-item has-text-primary has-text-weight-semibold">
            Stem Events
            </span>

            <a class="navbar-item" href="/">
                Calendar
            </a>

            <a class="navbar-item" href="/organizers">
                Organizers
            </a>
        </div>

        <div class="navbar-end">

            @guest
                <a class="navbar-item" href="{{ route('login') }}">
                Log in
                </a>
            @else
                <div class="navbar-item has-dropdown is-hoverable">
                    <span class="navbar-link">
                        <span style="margin-left: 10px;">{{ Auth::user()->first_name }}</span>
                    </span>
                    <div class="navbar-dropdown is-right">
                        <a href="#" class="navbar-item">
                            <span class="icon">
                                <i class="fas fa-user"></i>
                            </span>
                            Profile
                        </a>
                        <a href="{{ route('logout') }}"
                            onClick="event.preventDefault();
                                document.getElementById('logout-form').submit();"
                            class="navbar-item"
                        >
                            <span class="icon">
                                <i class="fas fa-sign-out-alt"></i>
                            </span>
                            Logout
                        </a>
            e            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                </div>
            @endguest
        </div>

    </div>
</nav>