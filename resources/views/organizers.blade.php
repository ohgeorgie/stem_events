@extends('layouts.app')

@section('content')
<div class="section">
    <organizers-list :organizers="{{ $organizers }}"></organizers-list>
</div>
@endsection