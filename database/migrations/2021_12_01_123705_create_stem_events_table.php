<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStemEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stem_events', function (Blueprint $table) {
            // $table->id();
            $table->unsignedBigInteger("id")->unique();
            $table->unsignedBigInteger("organizer_id")->nullable();
            $table->string("name")->nullable();
            $table->string("name_html")->nullable();
            $table->text("description")->nullable();
            $table->text("description_html")->nullable();
            $table->text("summary")->nullable();
            $table->dateTime("start_local")->nullable();
            $table->dateTime("start_utc")->nullable();
            $table->string("start_tz")->nullable();
            $table->dateTime("end_local")->nullable();
            $table->dateTime("end_utc")->nullable();
            $table->string("end_tz")->nullable();
            $table->boolean("online_event")->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stem_events');
    }
}
