<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrganizersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizers', function (Blueprint $table) {
            $table->unsignedBigInteger("id")->unique();
            $table->text("description")->nullable();
            $table->text("description_html")->nullable();
            $table->text("long_description")->nullable();
            $table->text("long_description_html")->nullable();
            $table->string("resource_uri")->nullable();
            $table->string("name")->nullable();
            $table->string("url")->nullable();
            $table->string("vanity_url")->nullable();
            $table->string("twitter")->nullable();
            $table->string("facebook")->nullable();
            $table->string("logo_id")->nullable();
            $table->boolean("is_active")->default(1);
            $table->datetime("last_scraped")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizers');
    }
}
