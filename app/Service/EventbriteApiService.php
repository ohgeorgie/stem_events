<?php

namespace App\Service;

use GuzzleHttp\Client;

class EventbriteApiService
{
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function run($uri, $type = 'GET', $args = [])
    {
        $args['token'] = env('EVENTBRITE_KEY');
        return json_decode($this->client->request($type, $uri, $args)->getBody());
    }

    public function getEvents($organizer_id)
    {
        $events = $this->run("v3/organizers/{$organizer_id}/events?status=live&token=ALLCNNHSFPJE36SXVFEE", "GET");
        return $events;
    }

    public function getOrganizer($organizer_id)
    {
        $organizer = $this->run("v3/organizers/{$organizer_id}/?token=ALLCNNHSFPJE36SXVFEE", "GET");
        return $organizer;
    }
}