<?php

namespace App\Http\Controllers;

use App\Models\Organizer;
use Illuminate\Http\Request;

class OrganizersController extends Controller
{
    public function index()
    {
        $organizers = Organizer::get()->keyBy('id');
        return view('organizers')->withOrganizers($organizers);
    }

    public function addOrUpdate($organizer_id)
    {
        $organizer = \Eventbrite::getOrganizer($organizer_id);
        $organizerInDB = Organizer::firstOrNew(['id' => $organizer_id]);

        $organizerInDB["description"] = $organizer->description->text;
        $organizerInDB["description_html"] = $organizer->description->html;
        $organizerInDB["long_description"] = $organizer->long_description->text;
        $organizerInDB["long_description_html"] = $organizer->long_description->html;
        $organizerInDB["resource_uri"] = $organizer->resource_uri;
        $organizerInDB["name"] = $organizer->name;
        $organizerInDB["url"] = $organizer->url;
        $organizerInDB["vanity_url"] = $organizer->vanity_url ?? null;
        $organizerInDB["twitter"] = $organizer->twitter ??null;
        $organizerInDB["facebook"] = $organizer->facebook ??null;
        $organizerInDB["logo_id"] = $organizer->logo_id;

        $organizerInDB->save();
        return $organizer;
    }
}
