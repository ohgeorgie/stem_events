<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Organizer;
use App\Models\StemEvent;
use Illuminate\Http\Request;

class ApiEventsController extends Controller
{
    public function index()
    {
        $events = StemEvent::get();
        return response(['events' => $events], 200);
    }

    public function fetchAllEvents()
    {
        $organizers = Organizer::where("is_active", 1)->get();

        foreach ($organizers as $organizer) {
            $this->fetchEventsFromEventbrite($organizer->id);
        }
        return response(["Message" => "Success"], 200);
    }

    private function fetchEventsFromEventbrite($organizer_id)
    {
        $events = \Eventbrite::getEvents($organizer_id);

        foreach ($events->events as $event) {
            $eventFromDB = StemEvent::firstOrNew(['id' => $event->id]);

            $eventFromDB->organizer_id = $event->organizer_id;
            $eventFromDB->name = $event->name->text;
            $eventFromDB->name_html = $event->name->html;
            $eventFromDB->description = $event->description->text;
            $eventFromDB->description_html = $event->description->html;
            $eventFromDB->summary = $event->summary;
            $eventFromDB->start_local = new Carbon($event->start->local, $event->start->timezone);
            $eventFromDB->start_utc = new Carbon($event->start->utc);
            $eventFromDB->start_tz = $event->start->timezone;
            $eventFromDB->end_local = new Carbon($event->end->local, $event->end->timezone);
            $eventFromDB->end_utc = new Carbon($event->end->utc);
            $eventFromDB->end_tz = $event->end->timezone;
            $eventFromDB->online_event = $event->online_event ? 1 : 0;

            $eventFromDB->save();
        }

        $organizer = Organizer::find($organizer_id);
        $organizer->last_scraped = Carbon::now();
        $organizer->save();
    }
}
