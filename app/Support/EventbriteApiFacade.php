<?php
namespace App\Support;

use Illuminate\Support\Facades\Facade;

class EventbriteApiFacade extends Facade
{
	protected static function getFacadeAccessor()
	{
		return 'eventbrite';
	}
}