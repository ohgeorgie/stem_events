<?php

namespace App\Models;

use App\Models\StemEvent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Organizer extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function stemEvents()
    {
        return $this->hasMany(StemEvent::class);
    }
}
