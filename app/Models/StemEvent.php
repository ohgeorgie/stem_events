<?php

namespace App\Models;

use App\Models\Organizer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class StemEvent extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $with = ['organizer'];

    public function organizer()
    {
        return $this->belongsTo(Organizer::class);
    }
}
