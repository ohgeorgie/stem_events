<?php

namespace App\Providers;

use GuzzleHttp\Client;
use App\Service\EventbriteApiService;
use Illuminate\Support\ServiceProvider;

class EventbriteApiProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('eventbrite', function () {
            $client = new Client([
                'base_uri' => 'https://www.eventbriteapi.com',
            ]);
            return new EventbriteApiService($client);
        });
    }

    public function boot()
    {
        //
    }
}